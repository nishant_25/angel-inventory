package com.angel.inventory.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InventoryUpdateRequest {

    @JsonProperty("product_id")
    String productId;
}
