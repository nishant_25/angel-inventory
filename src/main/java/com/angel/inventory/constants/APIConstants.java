package com.angel.inventory.constants;

public class APIConstants {
    public static final String INVENTORY = "/inventory";
    public static final String API = "/api";
    public static final String VERSION = "/v1";
    public static final String FETCH = "/fetch";
    public static final String UPDATE = "/update";
    public static final String HEALTH_CHECK = "/health";
}
