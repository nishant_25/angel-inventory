package com.angel.inventory.response;

import com.angel.inventory.db.entity.InventoryEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InventoryResponse{

    @JsonProperty("data")
    InventoryEntity data;

    @JsonProperty("message")
    String message;

    @JsonProperty("error")
    String error;

    @JsonProperty("status")
    Integer statusCode;
}
