package com.angel.inventory.service;

import com.angel.inventory.request.InventoryFetchRequest;
import com.angel.inventory.request.InventoryUpdateRequest;
import com.angel.inventory.response.InventoryResponse;
import org.springframework.http.ResponseEntity;

public interface InventoryService {

    ResponseEntity<InventoryResponse> getInventory(InventoryFetchRequest inventoryRequest);

    ResponseEntity<InventoryResponse> updateInventory(InventoryUpdateRequest inventoryUpdateRequest);

}
