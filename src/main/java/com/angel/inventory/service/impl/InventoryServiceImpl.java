package com.angel.inventory.service.impl;

import com.angel.inventory.db.entity.InventoryEntity;
import com.angel.inventory.db.repository.InventoryRepository;
import com.angel.inventory.request.InventoryFetchRequest;
import com.angel.inventory.request.InventoryUpdateRequest;
import com.angel.inventory.response.InventoryResponse;
import com.angel.inventory.service.InventoryService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
@Slf4j
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Override
    public ResponseEntity<InventoryResponse> getInventory(InventoryFetchRequest inventoryRequest) {
        try {
            InventoryResponse inventoryResponse = new InventoryResponse();
            InventoryEntity inventoryEntity = inventoryRepository.findByProductId(inventoryRequest.getProductId());
            inventoryResponse.setData(inventoryEntity);
            inventoryResponse.setMessage("Success");
            inventoryResponse.setError("");
            inventoryResponse.setStatusCode(200);
            return new ResponseEntity<>(inventoryResponse, HttpStatus.OK);
        } catch (Exception exception) {
            InventoryResponse inventoryResponse = new InventoryResponse();
            inventoryResponse.setData(null);
            inventoryResponse.setMessage("Error");
            inventoryResponse.setError(exception.getMessage());
            inventoryResponse.setStatusCode(500);
            return new ResponseEntity<>(inventoryResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public ResponseEntity<InventoryResponse> updateInventory(InventoryUpdateRequest inventoryUpdateRequest) {
        try {
            InventoryResponse inventoryResponse = new InventoryResponse();
            inventoryRepository.updateByProductId(inventoryUpdateRequest.getProductId());
            inventoryResponse.setData(null);
            inventoryResponse.setMessage("Success");
            inventoryResponse.setError("");
            inventoryResponse.setStatusCode(200);
            return new ResponseEntity<>(inventoryResponse, HttpStatus.OK);
        } catch (Exception exception) {
            InventoryResponse inventoryResponse = new InventoryResponse();
            inventoryResponse.setData(null);
            inventoryResponse.setMessage("Error");
            inventoryResponse.setError(exception.getMessage());
            inventoryResponse.setStatusCode(500);
            return new ResponseEntity<>(inventoryResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
