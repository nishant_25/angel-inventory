package com.angel.inventory.controller;

import com.angel.inventory.constants.APIConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(APIConstants.INVENTORY + APIConstants.API + APIConstants.VERSION)
@RequiredArgsConstructor
@Slf4j

public class HealthCheckController{

    @GetMapping(value = APIConstants.HEALTH_CHECK)
    public String healthCheckPath() {

        return "Health Check Passed";
    }
}
