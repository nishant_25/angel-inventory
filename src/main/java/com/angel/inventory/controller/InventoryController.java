package com.angel.inventory.controller;

import com.angel.inventory.constants.APIConstants;
import com.angel.inventory.request.InventoryFetchRequest;
import com.angel.inventory.request.InventoryUpdateRequest;
import com.angel.inventory.response.InventoryResponse;
import com.angel.inventory.service.InventoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(APIConstants.INVENTORY)
@RequiredArgsConstructor
@Slf4j
public class InventoryController {

    @Autowired
    InventoryService inventoryService;

    // Fetch Inventory
    @PostMapping(value = APIConstants.API + APIConstants.VERSION + APIConstants.FETCH, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<InventoryResponse> getInventory(@RequestBody InventoryFetchRequest inventoryRequest) {
        return inventoryService.getInventory(inventoryRequest);
    }

    // Update Inventory
    @PostMapping(value = APIConstants.API + APIConstants.VERSION + APIConstants.UPDATE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<InventoryResponse> updateInventory(@RequestBody InventoryUpdateRequest inventoryUpdateRequest) {
        return inventoryService.updateInventory(inventoryUpdateRequest);
    }

}
