package com.angel.inventory.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "inventory")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class InventoryEntity {

    @Id
    @Column(name = "product_id")
    @JsonProperty("product_id")
    String productId;

    @Column(name = "available_inventory")
    @JsonProperty("available_inventory")
    Long availableInventory;

    @Column(name = "created_at")
    @CreationTimestamp
    @JsonIgnore
    Timestamp createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    @JsonIgnore
    Timestamp updatedAt;
}
