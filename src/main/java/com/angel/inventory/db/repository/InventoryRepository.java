package com.angel.inventory.db.repository;


import com.angel.inventory.db.entity.InventoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;


public interface InventoryRepository extends JpaRepository<InventoryEntity, String> {

    @Query(value="select * from inventory where product_id = ?1", nativeQuery = true)
    InventoryEntity findByProductId(String productId);

    @Modifying
    @Transactional
    @Query(value="update inventory set available_inventory = available_inventory -1 where product_id = ?1", nativeQuery = true)
    void updateByProductId(String productId);


}
